import React, {Component} from "react";
import PropTypes from 'prop-types';
import styles from './Printer.module.scss';


class Printer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            printText: ''
        }
    }

    componentDidMount() {
        let sentenceDiv = document.getElementById('sentence');

        var that = this;

        function printSentence(sentence) {
            return new Promise((resolve, reject) => {
                sentence = sentence.split('');

                sentence.forEach((letter, index) => {
                        setTimeout(() => {
                            that.setState(state => ({
                                printText: state.printText + letter
                            }));
                            sentenceDiv.innerHTML = that.state.printText
                        }, that.props.miliseconds * index)
                    }
                );

                setTimeout(() => {
                    resolve('Printed!');
                }, sentence.length * that.props.miliseconds);


            });
        }
        
        printSentences(this,this.props.sentences);

        async function printSentences(that,sentences) {
            for (let index = 0; index < sentences.length; index++) {
                await printSentence(sentences[index]).then(print => {
                    that.setState(state => ({
                        printText: ''
                    }));
                });
            }
            printSentences(that,sentences);
        }

    }

    render() {
        return (
            <div id="sentence" className={styles.printer}/>
        )
    }

}


Printer.propTypes = {
    sentences: PropTypes.array.isRequired,
    miliseconds: PropTypes.number,
};

Printer.defaultProps = {
    miliseconds: 1000,
};

export default Printer;
