import React from 'react';
import logo from './logo.svg';
import './App.css';
import Printer from "./components/Printer/Printer";

function App() {

    let sentences = [
        'Lorem ipsum dolor sit amet',
        'consectetur adipiscing elit',
        'ed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        'Ut enim ad minim veniam,',
        'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat'
    ];


  return (
      <Printer sentences={sentences}/>
  );
}

export default App;
